require 'spec_helper'

describe 'highlight-tooltip', :js do
  before do
    visit '/handbook/values'
  end

  context 'twitter link' do
    it 'shows a valid tweet link in the tooltip' do
      credit_header = page.find('#credit')

      disable_animation

      # NOTE: end_position 1 select the entire text "CREDIT"
      select_text(credit_header, 0, 1)
      # TODO: Yes, sleeping is bad, but there seems to be some occasional flakiness
      # around the select_text not being immediate, which isn't fixed by disabling
      # animation. A small sleep is better than a flaky master.
      sleep 1

      fire_mouse_up_event(credit_header)

      highlight_tooltip = page.find('#highlight-tooltip')

      expect(highlight_tooltip).to have_link(href: %r{https://twitter.com/intent/tweet})
      expect(highlight_tooltip).to have_link(href: %r{url=.+/handbook/values})
      expect(highlight_tooltip).to have_link(href: /text=%22CREDIT%22/)
      expect(highlight_tooltip).to have_link(href: /hashtags=gitlab,handbook/)
    end
  end

  def disable_animation
    # prevent flaky tests - the built-in wait in Capybara's find doesn't seem to
    # always work when animations are involved

    js = <<-JAVASCRIPT
      document.querySelector('#highlight-tooltip').classList.add("js-disable-animation")
    JAVASCRIPT

    page.execute_script(js)
  end

  def select_text(element, start_position = 0, end_position = 0)
    js = <<-JAVASCRIPT
      const sel = window.getSelection()
      const range = window.document.createRange()
      const el = arguments[0]
      range.setStart(el,#{start_position})
      range.setEnd(el,#{end_position})
      sel.removeAllRanges()
      sel.addRange(range)
    JAVASCRIPT

    page.execute_script(js, element.native)
  end

  def fire_mouse_up_event(element)
    js = <<-JAVASCRIPT
      const el = arguments[0]
      const event = document.createEvent("HTMLEvents")
      event.initEvent("mouseup", true, true)
      event.eventName = "mouseup"
      el.dispatchEvent(event)
    JAVASCRIPT

    page.execute_script(js, element.native)
  end
end
