---
layout: handbook-page-toc
title: "Meltano Handbook"
---

[Meltano](https://meltano.com) enables anyone with access to SaaS APIs and spreadsheets to generate dashboards summarizing the status of their business operations.

Check out the Meltano [website](https://meltano.com/), [documentation](https://meltano.com/docs/), and [team handbook](https://meltano.com/handbook/) for more information.
